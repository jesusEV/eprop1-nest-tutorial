import numpy as np
from matplotlib import pyplot as plt
import plotfuncs_PRL as pf
import json
import argparse

sim_dict = {
        # nest results
        'loss_nest': {
            'paths': [
                './losses/loss_accuracy_error_400000_noloop.npy',
                './losses/loss_accuracy_error_400001_noloop.npy',
                './losses/loss_accuracy_error_400002_noloop.npy',
                './losses/loss_accuracy_error_400003_noloop.npy',
                './losses/loss_accuracy_error_400004_noloop.npy',
                './losses/loss_accuracy_error_400005_noloop.npy',
                './losses/loss_accuracy_error_400006_noloop.npy',
                './losses/loss_accuracy_error_400007_noloop.npy',
                './losses/loss_accuracy_error_400008_noloop.npy',
                './losses/loss_accuracy_error_400009_noloop.npy',
                './losses/loss_accuracy_error_400010_noloop.npy',
                './losses/loss_accuracy_error_400011_noloop.npy',
                './losses/loss_accuracy_error_400012_noloop.npy',
                './losses/loss_accuracy_error_400013_noloop.npy',
                './losses/loss_accuracy_error_400014_noloop.npy',
                './losses/loss_accuracy_error_400015_noloop.npy',
                './losses/loss_accuracy_error_400016_noloop.npy',
                './losses/loss_accuracy_error_400017_noloop.npy',
                './losses/loss_accuracy_error_400018_noloop.npy',
                './losses/loss_accuracy_error_400019_noloop.npy',
                './losses/loss_accuracy_error_400020_noloop.npy',
                './losses/loss_accuracy_error_400021_noloop.npy',
                './losses/loss_accuracy_error_400022_noloop.npy',
                './losses/loss_accuracy_error_400023_noloop.npy',
                './losses/loss_accuracy_error_400024_noloop.npy',
                './losses/loss_accuracy_error_400025_noloop.npy',
                './losses/loss_accuracy_error_400026_noloop.npy',
                './losses/loss_accuracy_error_400027_noloop.npy',
                './losses/loss_accuracy_error_400028_noloop.npy',
                './losses/loss_accuracy_error_400029_noloop.npy',
                ],
            'n_iter': 200,
            'color': 'tomato',
            'label': 'NEST',
            },
        # tf results.
        'loss_tf': {
            'paths': [
                './losses/results_tf_400000_test.json',
                './losses/results_tf_400001_test.json',
                './losses/results_tf_400002_test.json',
                './losses/results_tf_400003_test.json',
                './losses/results_tf_400004_test.json',
                './losses/results_tf_400005_test.json',
                './losses/results_tf_400006_test.json',
                './losses/results_tf_400007_test.json',
                './losses/results_tf_400008_test.json',
                './losses/results_tf_400009_test.json',
                './losses/results_tf_400010_test.json',
                './losses/results_tf_400011_test.json',
                './losses/results_tf_400012_test.json',
                './losses/results_tf_400013_test.json',
                './losses/results_tf_400014_test.json',
                './losses/results_tf_400015_test.json',
                './losses/results_tf_400016_test.json',
                './losses/results_tf_400017_test.json',
                './losses/results_tf_400018_test.json',
                './losses/results_tf_400019_test.json',
                './losses/results_tf_400020_test.json',
                './losses/results_tf_400021_test.json',
                './losses/results_tf_400022_test.json',
                './losses/results_tf_400023_test.json',
                './losses/results_tf_400024_test.json',
                './losses/results_tf_400025_test.json',
                './losses/results_tf_400026_test.json',
                './losses/results_tf_400027_test.json',
                './losses/results_tf_400028_test.json',
                './losses/results_tf_400029_test.json',
                ],
            'n_iter': 100,
            'color': 'cornflowerblue',
            'label': 'TensorFlow',
            },
        }

parser = argparse.ArgumentParser(description='plotting program')
parser.add_argument('--savefig', dest='path', default=None, const='../', nargs='?')
args = parser.parse_args()
#path = './data/'
# load nest results
simd = sim_dict['loss_nest']
n_files_loss_nest = len(simd['paths'])
nest_data = np.zeros((n_files_loss_nest, 3, simd['n_iter']))
for i, fname in enumerate(simd['paths']):
    data = np.load(fname)
    # data is of shape (3, n_iter) where the 0th dimesion contains (loss, accuracy, recall_error)
    nest_data[i,:,:] = np.array(data)[:,:simd['n_iter']]

# average and std dev over files dimension
nest_data_avg = np.mean(nest_data, axis=0)
nest_data_std = np.std(nest_data, axis=0)

# extract mean and std dev of loss and accuracy
loss_nest_avg = nest_data_avg[0]
loss_nest_std = nest_data_std[0]
accuracy_nest_avg = nest_data_avg[1]
accuracy_nest_std = nest_data_std[1]
nest_periods = np.arange(len(loss_nest_avg))

# load tf results
simd = sim_dict['loss_tf']
n_files_loss_tf = len(simd['paths'])
tf_data = np.zeros((n_files_loss_tf, 2, simd['n_iter']))
for i, fname in enumerate(simd['paths']):
    with open(fname, 'r') as d:
        data = json.load(d)
        tf_data[i,0,:] = np.array(data['loss_cls'])[:simd['n_iter']]# + np.array(data['loss_reg_f'])[:simd['n_iter']]
        tf_data[i,1,:] = np.array(data['accuracy'])[:simd['n_iter']]

# average and std dev over files dimension
tf_data_avg = np.mean(tf_data, axis=0)
tf_data_std = np.std(tf_data, axis=0)

# extract mean and std dev of loss and accuracy
loss_tf_avg = tf_data_avg[0]
loss_tf_std = tf_data_std[0]
accuracy_tf_avg = tf_data_avg[1]
accuracy_tf_std = tf_data_std[1]
tf_periods = np.arange(len(loss_tf_avg))

# define the number of periods to be plottted
n_periods = min(tf_periods[-1], nest_periods[-1])

'''
create figure
'''
width = 3.0 #8.0
lw = 1.5
lw_weights = 1.0
fs = 6
ms = 25
scale = 0.3 #0.6
n_h_panels = 1
n_v_panels = 2
p_h_factor = 0.6
p_w_factor = 1.0
p_h_off = 0.05
p_v_off = 0.00
alpha = 0.35
title_position = 'center'
panel_factory = pf.create_fig_PRL(
    1, scale, width, n_h_panels, n_v_panels, voffset=0.0, scale_height=0.6)
fig = panel_factory.return_figure()

# create panel for loss
ax_loss = panel_factory.new_panel(
    0, 0, '', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=-0.10, panel_width_factor=p_w_factor)

ax_loss.spines['right'].set_visible(False)
ax_loss.spines['top'].set_visible(False)

# plot TF loss
ax_loss.plot(tf_periods[:n_periods], loss_tf_avg[:n_periods], color=sim_dict['loss_tf']['color'], lw=lw, label=sim_dict['loss_tf']['label'])
ax_loss.fill_between(tf_periods[:n_periods], (loss_tf_avg - loss_tf_std)[:n_periods], (loss_tf_avg + loss_tf_std)[:n_periods],
        color=sim_dict['loss_tf']['color'], alpha=alpha)

# plot NEST loss
ax_loss.plot(nest_periods[:n_periods], loss_nest_avg[:n_periods], color=sim_dict['loss_nest']['color'], lw=lw, label=sim_dict['loss_nest']['label'])
ax_loss.fill_between(nest_periods[:n_periods], (loss_nest_avg - loss_nest_std)[:n_periods], (loss_nest_avg + loss_nest_std)[:n_periods],
        color=sim_dict['loss_nest']['color'], alpha=alpha)

ax_loss.set_xlim([0.0, n_periods + 1])
ax_loss.set_xticks([0.0, 50.0, 100.0])
ax_loss.set_xticklabels([])
ax_loss.set_ylim([0.48, 0.75])
ax_loss.set_yticks([0.5, 0.6, 0.7])
ax_loss.set_yticklabels(['0.5', '0.6', '0.7'])
ax_loss.set_ylabel('loss', fontsize=fs)

handles,labels = ax_loss.get_legend_handles_labels()
handles = [handles[1], handles[0]]
labels = [labels[1], labels[0]]
ax_loss.legend(handles, labels, ncol=2, fontsize=fs, loc='lower left')

# create panel for accuracy
ax_acc = panel_factory.new_panel(
    0, 1, '', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=0.15, panel_width_factor=p_w_factor)

ax_acc.spines['right'].set_visible(False)
ax_acc.spines['top'].set_visible(False)

# plot TF accuracy
ax_acc.plot(tf_periods[:n_periods], accuracy_tf_avg[:n_periods], color=sim_dict['loss_tf']['color'], lw=lw, label=sim_dict['loss_tf']['label'])
ax_acc.fill_between(tf_periods[:n_periods], (accuracy_tf_avg - accuracy_tf_std)[:n_periods], (accuracy_tf_avg + accuracy_tf_std)[:n_periods],
        color=sim_dict['loss_tf']['color'], alpha=alpha)

# plot NEST accuracy
ax_acc.plot(nest_periods[:n_periods], accuracy_nest_avg[:n_periods], color=sim_dict['loss_nest']['color'],
        lw=lw, label=sim_dict['loss_nest']['label'])
ax_acc.fill_between(nest_periods[:n_periods], (accuracy_nest_avg - accuracy_nest_std)[:n_periods], (accuracy_nest_avg +
    accuracy_nest_std)[:n_periods], color=sim_dict['loss_nest']['color'], alpha=alpha)

ax_acc.set_xlim([0.0, n_periods + 1])
ax_acc.set_xticks([0.0, 50.0, 100.0])
ax_acc.set_xticklabels(['0', '50', '100'])
ax_acc.set_ylim([0.47, 1.03])
ax_acc.set_yticks([0.5, 0.75, 1.0])
ax_acc.set_yticklabels(['0.5', '0.75', '1.0'])
ax_acc.set_xlabel('training iteration', fontsize=fs)
ax_acc.set_ylabel('accuracy', fontsize=fs)

if not args.path is None:
    figpath = args.path + 'fig_3_loss_accuracy_evid_acc'
    fig.savefig(figpath + '.svg')
    fig.savefig(figpath + '.pdf', dpi=600)
    print('saved figure as {} and {}'.format(figpath + '.svg', figpath + '.pdf'))
else:
    plt.show()
