import numpy as np
from matplotlib import pyplot as plt
import plotfuncs_PRL as pf
import json
import csv
import argparse

sim_dict = {
        'losses': {
            'path': './data/losses.npy',
            'labels': ['NEST', 'NEST (Dale)', 'TensorFlow'],
            },
        'weights': {
            'path': './data/weight_traces.json',
            },
        'weights_dale': {
            'path': './data/weight_traces_dale.json',
            },
        'traces_rec': {
            'path': './data/time_traces_rec_nest.npy',
            'from_iter': 0,
            'to_iter': 1,
            'indices': [4, 5], #[3, 4, 5],
            'seq_len': 1000,
            'labels': ['pre', 'post'], #[r'$4$', r'$5$', r'$6$', ],
            'colors': ['tomato', 'cornflowerblue'],
            },
        'traces_out': {
            'path': './data/time_traces_out_nest.npy',
            'from_iter': 0,
            'to_iter': 1,
            'seq_len': 1000,
            },
        'syn_traces_rec': {
            'path': './data/syn_traces.json',
            'idx_t': 0,
            'idx_pseudo': 1,
            'idx_z_hat': 2,
            'idx_elig': 3,
            'idx_grad': 4,
            'data_labels': ['104_to_105', '105_to_106', '106_to_104'],
            'labels': [r'pre $\rightarrow$ post'], #[r'$4 \rightarrow 5$', r'$5 \rightarrow 6$', r'$6 \rightarrow 4$', ],
            },
        'spikes_rec': {
            'path': './data/spike_times.json',
            'indices': [4, 5], #[3, 4, 5],
            'colors': ['firebrick', 'navy'], #['cornflowerblue', 'tomato'],
            },
        }

parser = argparse.ArgumentParser(description='plotting program')
parser.add_argument('--savefig', dest='path', default=None, const='../', nargs='?')
args = parser.parse_args()

# load loss
losses = np.load(sim_dict['losses']['path'])
loss_nest_mean = losses[0]
loss_nest_std = losses[1]
loss_nest_dale_mean = losses[2]
loss_nest_dale_std = losses[3]
loss_tf_mean = losses[4]
loss_tf_std = losses[5]

# load time traces of weights
with open(sim_dict['weights']['path'], 'r') as d:
    data = json.load(d)
    times_in = np.array(data['times_in'])
    weights_in = np.array(data['weights_in'])
    times_rec = np.array(data['times_rec'])
    weights_rec = np.array(data['weights_rec'])
    times_out = np.array(data['times_out'])
    weights_out = np.array(data['weights_out'])

# load time traces of weights with dale's law
with open(sim_dict['weights_dale']['path'], 'r') as d:
    data = json.load(d)
    times_dale_in = np.array(data['times_in'])
    weights_dale_in = np.array(data['weights_in'])
    times_dale_rec = np.array(data['times_rec'])
    weights_dale_rec = np.array(data['weights_rec'])
    times_dale_out = np.array(data['times_out'])
    weights_dale_out = np.array(data['weights_out'])

# load recurrent time traces
traces_rec = np.load(sim_dict['traces_rec']['path'])
times_tr_rec = traces_rec[0]
Vms_rec = traces_rec[1]
pseudo_deriv_rec = traces_rec[2]
learning_signal_rec = traces_rec[3]

# load output time traces
traces_out = np.load(sim_dict['traces_out']['path'])
times__tr_out = traces_out[0]
Vms_out = traces_out[1]
learning_signal_out = traces_out[2]
target_rate = traces_out[3]

# load synaptic time traces
simd = sim_dict['syn_traces_rec']
with open(simd['path'], 'r') as d:
    data = json.load(d)
    for k, v in data.items():
        simd[k] = np.array(v)[0]

# load synaptic time traces
simd = sim_dict['spikes_rec']
with open(simd['path'], 'r') as d:
    data = json.load(d)
    for k, v in data.items():
        simd[k] = np.array(v)

width = 4.0
lw = 1.5
lw_weights = 1.0
fs = 10
ms = 25
scale = 0.6
n_h_panels = 6
n_v_panels = 3
p_h_factor = 3.7
p_w_factor = 1.70
p_h_off = 0.03
p_h_off_r = 0.01
p_v_off = 0.45
p_v_off_dale = 0.30
alpha = 0.35
title_position = 'center'
colors_dict = {
        'loss_nest': 'tomato',
        'loss_nest_dale': 'mediumseagreen',
        'loss_tf': 'cornflowerblue',
        'weights': 'tomato',#'grey',
        'weights_dale': 'mediumseagreen',#'grey',
        'weights_hl': 'mediumseagreen',
        'Vm_rec': 'tomato',
        'pseudo_deriv': 'cornflowerblue',
        'ls_rec': 'grey',
        'Vm_out': 'firebrick',
        'ls_out': 'grey',
        'target': 'gold',
        'elig': 'mediumseagreen',
        'grad': 'mediumseagreen',
        }

def prep_cutted_plot(axl, axr, xlims, xticks, xtick_labels):
    axl.set_xlim(xlims[:2])
    axr.set_xlim(xlims[2:])
    assert (len(xticks) == len(xtick_labels)) or (len(xtick_labels) == 0), 'len(xticks) must equal len(xtick_labels)!'
    x_labels_half = len(xticks) // 2
    axl.set_xticks(xticks[:x_labels_half])
    axr.set_xticks(xticks[x_labels_half:])
    if len(xtick_labels):
        axl.set_xticklabels(xtick_labels[:x_labels_half])
        axr.set_xticklabels(xtick_labels[x_labels_half:])
    else:
        axl.set_xticklabels([])
        axr.set_xticklabels([])

    # hide the spines between axl and axr
    axl.spines['right'].set_visible(False)
    axl.spines['top'].set_visible(False)
    axl.yaxis.tick_left()
    #axl.tick_params(labelright='off')
    #axr.yaxis.tick_right()
    axr.get_yaxis().set_visible(False)
    axr.get_yaxis().set_ticks([])
    #axr.set_yticklabels([])
    axr.spines['left'].set_visible(False)
    axr.spines['right'].set_visible(False)
    axr.spines['top'].set_visible(False)
    d = .015 # how big to make the diagonal lines in axes coordinates
    kwargs = dict(transform=axl.transAxes, color='k', clip_on=False)
    axl.plot((1-d,1+d), (-d,+d), **kwargs)
    kwargs.update(transform=axr.transAxes)
    axr.plot((-d,+d), (-d,+d), **kwargs)
    axl.xaxis.set_label_coords(1.10, -0.255)

'''
create figure for weights and loss
'''
panel_factory = pf.create_fig_PRL(
    1, scale, width, n_h_panels, n_v_panels, voffset=0.0, scale_height=4.0, squeeze=0.17)
fig = panel_factory.return_figure()

ax_weights_in = panel_factory.new_panel(
    0, 0, 'A', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off, panel_width_factor=p_w_factor)

ax_weights_rec = panel_factory.new_panel(
    2, 0, 'B', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off, panel_width_factor=p_w_factor)

ax_weights_out = panel_factory.new_panel(
    4, 0, 'C', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off, panel_width_factor=p_w_factor)

ax_weights_dale_in = panel_factory.new_panel(
    0, 1, 'D', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off_dale, panel_width_factor=p_w_factor)

ax_weights_dale_rec = panel_factory.new_panel(
    2, 1, 'E', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off_dale, panel_width_factor=p_w_factor)

ax_weights_dale_out = panel_factory.new_panel(
    4, 1, 'F', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off_dale, panel_width_factor=p_w_factor)

ax_loss = panel_factory.new_panel(
    0, 2, 'G', label_position='leftleft', panel_height_factor=p_h_factor,
    hoffset=p_h_off + 0.00, voffset=0.1, panel_width_factor=3.8*p_w_factor)

# synaptic weights
hl_indices = [0, 1, 3]
xlim = [0.0, 2.0e6]
xlims = [0.0, 0.5e6, 1.5e6, 2.0e6]
xticks = [0.0, 1.0e6, 2.0e6]
xtick_labels = ['0', '1000', '2000']
xtick_labels_split = ['0', '0.5', '1.5', '2']
alpha_weights = 0.8
# randomly select n_random weights to be plotted
rng = np.random.default_rng(302687)
n_random = 25
# input weights
rnd_idx = rng.choice(len(weights_in), n_random, replace=False)
for ts, tsd, ws, wsd in zip(times_in[rnd_idx], times_dale_in[rnd_idx], weights_in[rnd_idx], weights_dale_in[rnd_idx]):
    ts = np.insert(ts, 0, xlim[0])
    ws = np.insert(ws, 0, ws[0])
    ts = np.append(ts, xlim[-1])
    ws = np.append(ws, ws[-1])
    ax_weights_in.step(ts, ws, lw=lw_weights, color=colors_dict['weights'], alpha=alpha_weights)
    tsd = np.append(tsd, xlim[-1])
    wsd = np.append(wsd, wsd[-1])
    tsd = np.append(tsd, xlim[-1])
    wsd = np.append(wsd, wsd[-1])
    ax_weights_dale_in.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_dale'], alpha=alpha_weights)

'''
for ts, tsd, ws, wsd in zip(times_in[hl_indices], times_dale_in[hl_indices], weights_in[hl_indices], weights_dale_in[hl_indices]):
    ax_weights_in.step(ts, ws, lw=lw_weights, color=colors_dict['weights_hl'])
    ax_weights_dale_in.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_hl'])
'''

ax_weights_in.set_xlim(xlim)
ax_weights_in.set_xticks(xticks)
ax_weights_in.set_xticklabels([])
ax_weights_in.set_ylabel('syn. weights', fontsize=fs)
#ax_weights_in.set_title('input weights', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

ax_weights_dale_in.set_xlim(xlim)
ax_weights_dale_in.set_xticks(xticks)
ax_weights_dale_in.set_xticklabels(xtick_labels)
ax_weights_dale_in.set_xlabel('training iteration', fontsize=fs)
ax_weights_dale_in.set_ylabel('syn. weights', fontsize=fs)
#ax_weights_dale_in.set_title('input weights (dale)', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

# recurrent weights
rnd_idx = rng.choice(len(weights_rec), n_random, replace=False)
for ts, tsd, ws, wsd in zip(times_rec[rnd_idx], times_dale_rec[rnd_idx], weights_rec[rnd_idx], weights_dale_rec[rnd_idx]):
    ts = np.insert(ts, 0, xlim[0])
    ws = np.insert(ws, 0, ws[0])
    ts = np.append(ts, xlim[-1])
    ws = np.append(ws, ws[-1])
    ax_weights_rec.step(ts, ws, lw=lw_weights, color=colors_dict['weights'], alpha=alpha_weights)
    tsd = np.append(tsd, xlim[-1])
    wsd = np.append(wsd, wsd[-1])
    tsd = np.append(tsd, xlim[-1])
    wsd = np.append(wsd, wsd[-1])
    ax_weights_dale_rec.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_dale'], alpha=alpha_weights)

'''
for ts, tsd, ws, wsd in zip(times_rec[hl_indices], times_dale_rec[hl_indices], weights_rec[hl_indices], weights_dale_rec[hl_indices]):
    ax_weights_rec.step(ts, ws, lw=lw_weights, color=colors_dict['weights_hl'])
    ax_weights_dale_rec.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_hl'])
'''

ax_weights_rec.set_xlim(xlim)
ax_weights_rec.set_xticks(xticks)
ax_weights_rec.set_xticklabels([])
#ax_weights_rec.set_title('recurrent weights', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

ax_weights_dale_rec.set_xlim(xlim)
ax_weights_dale_rec.set_xticks(xticks)
ax_weights_dale_rec.set_xticklabels(xtick_labels)
ax_weights_dale_rec.set_xlabel('training iteration', fontsize=fs)
#ax_weights_dale_rec.set_title('recurrent weights (dale)', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

# output weights
rnd_idx = rng.choice(len(weights_out), n_random, replace=False)
for ts, tsd, ws, wsd in zip(times_out[rnd_idx], times_dale_out[rnd_idx], weights_out[rnd_idx], weights_dale_out[rnd_idx]):
    if len(ws):
        ts = np.insert(ts, 0, xlim[0])
        ws = np.insert(ws, 0, ws[0])
        ts = np.append(ts, xlim[-1])
        ws = np.append(ws, ws[-1])

    ax_weights_out.step(ts, ws, lw=lw_weights, color=colors_dict['weights'], alpha=alpha_weights)
    if len(wsd):
        tsd = np.insert(tsd, 0, xlim[0])
        wsd = np.insert(wsd, 0, wsd[0])
        tsd = np.append(tsd, xlim[-1])
        wsd = np.append(wsd, wsd[-1])

    ax_weights_dale_out.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_dale'], alpha=alpha_weights)

'''
for ts, tsd, ws, wsd in zip(times_out[hl_indices], times_dale_out[hl_indices], weights_out[hl_indices], weights_dale_out[hl_indices]):
    ax_weights_out.step(ts, ws, lw=lw_weights, color=colors_dict['weights_hl'])
    ax_weights_dale_out.step(tsd, wsd, lw=lw_weights, color=colors_dict['weights_hl'])
'''

ax_weights_out.set_xlim(xlim)
ax_weights_out.set_xticks(xticks)
ax_weights_out.set_xticklabels([])
#ax_weights_out.set_title('output weights', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

ax_weights_dale_out.set_xlim(xlim)
ax_weights_dale_out.set_xticks(xticks)
ax_weights_dale_out.set_xticklabels(xtick_labels)
ax_weights_dale_out.set_xlabel('training iteration', fontsize=fs)
#ax_weights_dale_out.set_title('output weights (dale)', y=0.95, loc=title_position, fontdict={'fontsize': fs-2})

# loss
step_loss = 10
times_loss = step_loss*np.arange(len(loss_tf_mean))
# TF
simd = sim_dict['losses']
ax_loss.plot(times_loss, loss_tf_mean, color=colors_dict['loss_tf'], lw=lw+1, label=simd['labels'][2])
ax_loss.fill_between(times_loss, (loss_tf_mean - loss_tf_std), (loss_tf_mean + loss_tf_std),
        color=colors_dict['loss_tf'], alpha=alpha)
# NEST Dale's law
ax_loss.plot(times_loss, loss_nest_dale_mean, color=colors_dict['loss_nest_dale'], lw=lw, ls='-', label=simd['labels'][1])
ax_loss.fill_between(times_loss, (loss_nest_dale_mean - loss_nest_dale_std), (loss_nest_dale_mean + loss_nest_dale_std),
        color=colors_dict['loss_nest_dale'], alpha=alpha)
# NEST
ax_loss.plot(times_loss, loss_nest_mean, color=colors_dict['loss_nest'], lw=lw-0.5, ls='-', label=simd['labels'][0])
ax_loss.fill_between(times_loss, (loss_nest_mean - loss_nest_std), (loss_nest_mean + loss_nest_std),
        color=colors_dict['loss_nest'], alpha=alpha)

ax_loss.set_xlim([0.0, 2000.0])
ax_loss.set_xticks([0.0, 500.0, 1000.0, 1500.0, 2000.0])
ax_loss.set_xticklabels(['0', '500', '1000', '1500', '2000'])

ax_loss.set_yscale("log", nonpositive='clip')
ax_loss.set_ylim([2.0, 200.0])
ax_loss.set_yticks([10.0, 100.0])
ax_loss.set_yticklabels(['10', '100'])
ax_loss.set_xlabel('training iteration', fontsize=fs)
ax_loss.set_ylabel('loss', fontsize=fs)
handles,labels = ax_loss.get_legend_handles_labels()
handles = [handles[2], handles[1], handles[0]]
labels = [labels[2], labels[1], labels[0]]
ax_loss.legend(handles, labels, ncol=3, fontsize=fs-4, loc='upper center', handlelength=4)

'''
create figure for membrane potential, learning signal, pseudo derivative, ...
'''
seq_len = 1000

lw = 1.0
width = 6.0
n_h_panels = 1
n_v_panels = 6
p_h_factor = 0.27
p_w_factor = 1.19
p_h_off = -0.02
p_v_off = 0.09
step_off = -0.28
title_position = 'center'

panel_factory2 = pf.create_fig_PRL(
    2, scale, width, n_h_panels, n_v_panels, voffset=0.0, scale_height=0.3)
fig2 = panel_factory2.return_figure()

ax_Vm_rec = panel_factory2.new_panel(
    0, 0, 'A', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+5*step_off, panel_width_factor=p_w_factor)

ax_pseudo_deriv = panel_factory2.new_panel(
    0, 1, 'B', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+4*step_off, panel_width_factor=p_w_factor)

ax_elig_rec = panel_factory2.new_panel(
    0, 2, 'C', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+3*step_off, panel_width_factor=p_w_factor)

ax_grad_rec = panel_factory2.new_panel(
    0, 3, 'D', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+2*step_off, panel_width_factor=p_w_factor)

ax_Vm_out = panel_factory2.new_panel(
    0, 4, 'E', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off+1*step_off, panel_width_factor=p_w_factor)

ax_ls_out = panel_factory2.new_panel(
    0, 5, 'F', label_position='left', panel_height_factor=p_h_factor,
    hoffset=p_h_off, voffset=p_v_off, panel_width_factor=p_w_factor)


simd = sim_dict['traces_rec']
from_ind = simd['seq_len']*simd['from_iter']
to_ind = simd['seq_len']*simd['to_iter']
times_rec = np.arange(to_ind - from_ind)

alphas = np.linspace(0.4, 1.0, len(simd['indices']))[::-1]
for (nrn_ind, label, color, alpha) in zip(simd['indices'], simd['labels'], simd['colors'], alphas):
    ax_Vm_rec.plot(times_rec, Vms_rec[nrn_ind,from_ind:to_ind], lw=lw, color=color, zorder=1, label=label)
    ax_pseudo_deriv.plot(times_rec, pseudo_deriv_rec[nrn_ind,from_ind:to_ind], lw=lw, color=color, label=label)

ax_Vm_rec.set_xlim([times_rec[0], times_rec[-1]])
ax_Vm_rec.set_xticks([0.0, 500.0, 1000.0])
ax_Vm_rec.set_xticklabels([])
ax_Vm_rec.legend(ncol=3, fontsize=fs-2, loc='upper left')
ax_Vm_rec.set_title(r'$V_{m}$ and spikes', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

simd = sim_dict['spikes_rec']
n_idx = len(simd['indices'])
alphas = np.linspace(0.4, 1.0, n_idx)[::-1]
for i, (nrn_ind, color, alpha) in enumerate(zip(simd['indices'], simd['colors'], alphas)):
    for t in np.mod(np.array(simd['spike_times'][nrn_ind]), 1000):
        ax_Vm_rec.axvline(t - 1, i / float(n_idx), (i+1) / float(n_idx), lw=lw, color=color, zorder=2.5)

ax_pseudo_deriv.set_xlim([times_rec[0], times_rec[-1]])
ax_pseudo_deriv.set_xticks([0.0, 500.0, 1000.0])
ax_pseudo_deriv.set_xticklabels([])
#ax_pseudo_deriv.legend(ncol=3, fontsize=fs-4, loc='upper left')
ax_pseudo_deriv.set_title('pseudo derivative', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

simd = sim_dict['syn_traces_rec']
alphas = np.linspace(0.4, 1.0, len(simd['data_labels']))[::-1]
for (k, label, alpha) in zip(simd['data_labels'], simd['labels'], alphas):
    syn_data = simd[k]
    ax_elig_rec.plot(np.insert((syn_data[simd['idx_t']] - 2) % 1000, 0, 0.0), np.insert(syn_data[simd['idx_elig']], 0, 0.0),
            lw=lw, color=colors_dict['elig'], alpha=alpha, label=label)
    #ax_elig_rec.plot(syn_data[simd['idx_t']] % 1002, syn_data[simd['idx_pseudo']], lw=lw, color=colors_dict['target'],
    #        alpha=alpha)
    #ax_elig_rec.plot((syn_data[simd['idx_t']] % 1002) - 2, syn_data[simd['idx_z_hat']], lw=lw, color=colors_dict['ls_rec'],
    #        alpha=alpha)
    ax_grad_rec.plot(np.insert((syn_data[simd['idx_t']] - 2) % 1000 - 2, 0, 0.0), np.insert(syn_data[simd['idx_grad']], 0, 0.0),
            lw=lw, color=colors_dict['grad'], alpha=alpha, label=label)

ax_elig_rec.set_xlim([times_rec[0], times_rec[-1]])
ax_elig_rec.set_xticks([0.0, 500.0, 1000.0])
ax_elig_rec.set_xticklabels([])
ax_elig_rec.legend(ncol=3, fontsize=fs-2, loc='upper left')
ax_elig_rec.set_title('eligibility trace', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

ax_grad_rec.set_xlim([times_rec[0], times_rec[-1]])
ax_grad_rec.set_xticks([0.0, 500.0, 1000.0])
ax_grad_rec.set_xticklabels([])
#ax_grad_rec.legend(ncol=3, fontsize=fs-4, loc='upper left')
ax_grad_rec.set_title('cumulated gradient', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

simd = sim_dict['traces_out']
from_ind = simd['seq_len']*simd['from_iter']
to_ind = simd['seq_len']*simd['to_iter']
times_out = np.arange(to_ind - from_ind)

ax_Vm_out.plot(times_out, Vms_out[0,from_ind:to_ind], lw=lw, color=colors_dict['Vm_out'], label=r'$y$')
ax_Vm_out.plot(times_out, target_rate[0,from_ind:to_ind], lw=lw, color=colors_dict['target'], label=r'$y^{\ast}$')
ax_ls_out.plot(times_out, learning_signal_out[0,from_ind:to_ind], lw=lw, color=colors_dict['ls_out'], label=r'$y - y^{\ast}$')

ax_Vm_out.set_xlim([times_rec[0], times_rec[-1]])
ax_Vm_out.set_xticks([0.0, 500.0, 1000.0])
ax_Vm_out.set_xticklabels([])
ax_Vm_out.legend(ncol=2, fontsize=fs-2, loc='upper left')
ax_Vm_out.set_title('output and target', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

ax_ls_out.set_xlim([times_rec[0], times_rec[-1]])
ax_ls_out.set_xticks([0.0, 500.0, 1000.0])
ax_ls_out.set_xticklabels(['0', '500', '1000'])
ax_ls_out.set_xlabel(r'$t$ $[\mathrm{ms}]$', fontsize=fs)
ax_ls_out.legend(ncol=1, fontsize=fs-2, loc='upper left')
ax_ls_out.set_title('error sigal', y=0.9, loc=title_position, fontdict={'fontsize': fs-2})

if not args.path is None:
    figpath = args.path + 'fig_2_weights_loss'
    fig.savefig(figpath + '.svg')
    fig.savefig(figpath + '.pdf', dpi=600)
    print('saved figure as {} and {}'.format(figpath + '.svg', figpath + '.pdf'))
    figpath = args.path + 'fig_1_traces'
    fig2.savefig(figpath + '.svg')
    fig2.savefig(figpath + '.pdf', dpi=600)
    print('saved figure as {} and {}'.format(figpath + '.svg', figpath + '.pdf'))
else:
    plt.show()

