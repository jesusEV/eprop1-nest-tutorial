import numpy as np
import numpy.ma as ma
import nest
from matplotlib import pyplot as plt
import json
import time
import datetime
from models_nest import sum_of_sines_target

'''
simulation parameters
'''
# temporal resolution of the simulation
resolution = 1.0
# dendritic delay of all synapses
delay = resolution
# firing rate of frozen input noise
input_f0 = 50. / 1000.
# number of input neurons
n_in = 100
# number of recurrent neurons
n_rec = 100
# number of output neurons
n_out = 1
# number of iterations
n_iter = 101
n_iter_start_recording = n_iter - 2
n_iter_stop_recording = n_iter_start_recording + 1
# record from synape at learning_period_counter_ = iteration n_iter - 1
# duration of one training period
seq_len = 1000
# total simulation time
sim_time = n_iter * seq_len + 5.0
# interval for updating the synapse weights
update_interval = float(seq_len)
# learning rate
learning_rate = 1.0e-4
# prefactor of firing rate regularization
reg = 300.0
# number of virtual processes for the simulation
nvp = 1
# fix numpy random seed
rnd_seed = 302687
np.random.seed(rnd_seed)
# record spikes of recurrent neurons only for small networks and short sim times
record_spikes = True

t_start = time.time()
'''
reset NEST kernel
'''
nest.ResetKernel()
nest.SetKernelStatus({'resolution': resolution, 'total_num_virtual_procs': nvp,
                      'print_time': False})

'''
definition of model names
'''
in_model = 'parrot_neuron'
rec_model = 'iaf_psc_delta_eprop'
out_model = 'error_neuron'
# rate generators provide the target signal
rg_model = 'step_rate_generator'
# spike generators provide the frozen input noise
sg_model = 'spike_generator'
# broadcast model -> random feedback weights
broadcast_model = 'learning_signal_connection_delayed'
# neuron parameters
rec_neuron_params = {
    'V_m': 0.0,
    'V_th': 0.03,
    'V_reset': 0.0,
    'E_L': 0.0,
    'tau_m': 30.0,
    't_ref': 0.0,
    'dampening_factor': 0.3,
    'update_interval': update_interval,
}
out_neuron_params = {
    'V_m': 0.0,
    'E_L': 0.0,
    'tau_m': 30.0,
    'dampening_factor': 1.0,
    'update_interval': update_interval,
}

'''
draw random weights
'''
rng = np.random.default_rng(302687)
in_weights = rng.standard_normal((n_in-1, n_rec)) / np.sqrt(n_in)
in_weights_wr = rng.standard_normal((1, n_rec)) / np.sqrt(n_in)
rec_weights = rng.standard_normal((n_rec-1, n_rec)) / np.sqrt(n_rec)
rec_weights_wr = rng.standard_normal((1, n_rec)) / np.sqrt(n_rec)
# set diagonal to zero (no autapses)
np.fill_diagonal(rec_weights, 0.0)
out_weights = rng.standard_normal((n_rec, n_out)) / np.sqrt(n_rec)
rand_feedback_weights = rng.standard_normal((n_rec, n_out)) / np.sqrt(n_rec)

'''
create neurons and devices
'''
in_nrns = nest.Create(in_model, n_in)
rec_nrns = nest.Create(rec_model, n_rec, params=rec_neuron_params)
out_nrns = nest.Create(out_model, n_out, params=out_neuron_params)
'''
generate frozen noise input
'''
rng = np.random.default_rng(302687)
frozen_poisson_noise_input = (rng.random((seq_len, n_in)) < resolution * input_f0).T
sg_params = []
for spks in frozen_poisson_noise_input:
    spktimes = np.array(np.unique(spks * np.arange(seq_len))[1:], dtype=np.float32)
    sg_params.append({'spike_times': np.hstack([spktimes + j * seq_len
                                                for j in range(n_iter)])})

spike_generators = nest.Create(sg_model, n_in, params=sg_params)
# create target signal
target_sinusoidal_outputs = np.tile([sum_of_sines_target(seq_len, rnd_seed) for i in range(n_out)], n_iter)
rg_times = np.arange(resolution, n_iter * seq_len + 1, resolution)

rg_params = []
for ts in target_sinusoidal_outputs:
    rg_params.append({'amplitude_times': rg_times, 'amplitude_values': ts})

rate_generators = nest.Create(rg_model, n_out, params=rg_params)
# multimeter that records the membrane potential and the target signal of the outpu neuron
mm_rec = nest.Create('multimeter', params={'interval': resolution, 'start': float(seq_len*n_iter_start_recording),
    'stop': float(seq_len*n_iter_stop_recording) + 2, 'record_from': ['V_m', 'V_th', 'learning_signal']})
mm_out = nest.Create('multimeter', params={'interval': resolution, 'start': float(seq_len*n_iter_start_recording),
    'stop': float(seq_len*n_iter_stop_recording) + 2, 'record_from': ['V_m', 'learning_signal', 'target_rate']})

wr_in = nest.Create('weight_recorder')
wr_rec = nest.Create('weight_recorder')
wr_out = nest.Create('weight_recorder')
if record_spikes:
    sd = nest.Create('spike_recorder', 1, params={'start': float(seq_len*n_iter_start_recording),
        'stop': float(seq_len*n_iter_stop_recording)})

# create connections

def create_Wmin_Wmax(weight_matrix):
    Wmin = np.zeros_like(weight_matrix)
    Wmax = np.zeros_like(weight_matrix)

    signs = np.sign(weight_matrix)

    Wmin[np.where(signs == -1.)] = -100.
    Wmax[np.where(signs == 1.)] = 100.
    return Wmin, Wmax

Wmin_in, Wmax_in = create_Wmin_Wmax(in_weights.T)
Wmin_in_wr, Wmax_in_wr = create_Wmin_Wmax(in_weights_wr.T)
Wmin_rec, Wmax_rec = create_Wmin_Wmax(rec_weights.T)
Wmin_rec_wr, Wmax_rec_wr = create_Wmin_Wmax(rec_weights_wr.T)
Wmin_out, Wmax_out = create_Wmin_Wmax(out_weights.T)


nest.CopyModel('eprop_synapse','eprop_synapse_wr_in', {"weight_recorder": wr_in})
nest.CopyModel('eprop_synapse','eprop_synapse_wr_rec', {"weight_recorder": wr_rec})
nest.CopyModel('eprop_synapse','eprop_synapse_wr_out', {"weight_recorder": wr_out})
eprop_model = 'eprop_synapse'

synapse_in = {'synapse_model': eprop_model, 'learning_rate': learning_rate,
              'weight': in_weights.T, 'delay': delay, 'update_interval': update_interval,
              'tau_decay': 30.0, "keep_traces": 0.0,
              'target_firing_rate': 10.0, 'rate_reg': reg, 'Wmin': Wmin_in, 'Wmax': Wmax_in}
synapse_rec = {'synapse_model': eprop_model, 'learning_rate': learning_rate,
               "weight": rec_weights.T, 'delay': delay, 'update_interval': update_interval,
               'tau_decay': 30.0, 'keep_traces': 0.0,
               'target_firing_rate': 10.0, 'rate_reg': reg, 'Wmin': Wmin_rec, 'Wmax': Wmax_rec}
synapse_out = {'synapse_model': 'eprop_synapse_wr_out',
               'weight': out_weights.T, 'delay': delay, 'tau_decay': 30.0, 'update_interval': update_interval,
               'keep_traces': 0., 'learning_rate': learning_rate, 'Wmin': Wmin_out, 'Wmax': Wmax_out}
synapse_broadcast = {'synapse_model': broadcast_model,
                     'weight': rand_feedback_weights, 'delay': delay}
synapse_target = {'synapse_model': 'rate_connection_delayed',
                  'weight': 1., 'delay': delay, 'receptor_type': 2}
static_synapse = {'synapse_model': 'static_synapse',
                  'weight': 1.0, 'delay': delay}
conn_spec = {'rule': 'all_to_all', 'allow_autapses': False}

# NEST does not support a direct connection of devises to neurons via plastic synapses. This is why we have the parrot
# neurons between the spike generators and the recurrent network. Parrot neurons simply send a spike whenever they are
# receiving one.
nest.Connect(spike_generators, in_nrns, syn_spec=static_synapse, conn_spec={'rule': 'one_to_one'})
# we attach one weight recorder to all connections from one input/recurrent neuron and another one
# to all incoming connections of the output neuron
nest.Connect(in_nrns[1:], rec_nrns, syn_spec=synapse_in, conn_spec=conn_spec)
synapse_in['synapse_model'] = 'eprop_synapse_wr_in'
synapse_in['weight'] = in_weights_wr.T
synapse_in['Wmax'] = Wmax_in_wr
synapse_in['Wmin'] = Wmin_in_wr
nest.Connect(in_nrns[1], rec_nrns, syn_spec=synapse_in, conn_spec=conn_spec)
nest.Connect(rec_nrns[:-1], rec_nrns, syn_spec=synapse_rec, conn_spec=conn_spec)
synapse_rec['synapse_model'] = 'eprop_synapse_wr_rec'
synapse_rec['weight'] = rec_weights_wr.T
synapse_rec['Wmax'] = Wmax_rec_wr
synapse_rec['Wmin'] = Wmin_rec_wr
nest.Connect(rec_nrns[-1], rec_nrns, syn_spec=synapse_rec, conn_spec=conn_spec)
nest.Connect(rec_nrns, out_nrns, syn_spec=synapse_out, conn_spec=conn_spec)
nest.Connect(out_nrns, rec_nrns, syn_spec=synapse_broadcast, conn_spec=conn_spec)
nest.Connect(rate_generators, out_nrns, syn_spec=synapse_target, conn_spec={'rule': 'one_to_one'})

nest.Connect(mm_out, out_nrns, syn_spec=static_synapse)
nest.Connect(mm_rec, rec_nrns, syn_spec=static_synapse)

if record_spikes:
    nest.Connect(rec_nrns, sd, syn_spec=static_synapse)

'''
run simulation
'''
nest.Simulate(sim_time)

'''
process data of recording devices
'''
times_all = np.arange(n_iter * seq_len)
if record_spikes:
    # compute average firing rate
    spikes_events = nest.GetStatus(sd, 'events')[0]
    senders_rec = spikes_events['senders']
    rec_spike_times = np.array(spikes_events['times']) - 2.0 * delay
    rec_spite_times_per_neuron = [rec_spike_times[np.where(senders_rec == i)].tolist() for i in rec_nrns.tolist()]
    res_dict = {
            'senders': rec_nrns.tolist(),
            'spike_times': rec_spite_times_per_neuron,
            }
    datapath = './data/'
    filename = 'spike_times.json'
    with open(datapath + filename, 'w') as f:
        json.dump(res_dict, f)

# readout of multimeter connected to recurrent neurons
events_rec = nest.GetStatus(mm_rec)[0]['events']
senders_rec = events_rec['senders']
times_rec = np.array([events_rec['times'][np.where(senders_rec == i)][2:2 + n_iter * seq_len] for i in
    rec_nrns.tolist()])
Vms_rec = np.array([events_rec['V_m'][np.where(senders_rec == i)][2:2 + n_iter * seq_len] for i in
    rec_nrns.tolist()])
pseudo_deriv_rec = np.array([events_rec['V_th'][np.where(senders_rec == i)][2:2 + n_iter * seq_len] for i in
    rec_nrns.tolist()])
learning_signal_rec = np.array([events_rec['learning_signal'][np.where(senders_rec == i)][2:2 + n_iter * seq_len] for i in
    rec_nrns.tolist()])
results_rec = np.array([times_rec, Vms_rec, pseudo_deriv_rec, learning_signal_rec])
np.save('./data/time_traces_rec_nest.npy', results_rec)

# membrane potential of readout neuron
events_out = nest.GetStatus(mm_out)[0]['events']
senders_out = events_out['senders']
Vms_out = [events_out['V_m'][np.where(senders_out == i)][2:2 + n_iter * seq_len] for i in out_nrns.tolist()]
times_out = np.array([events_out['times'][np.where(senders_out == i)][2:2 + n_iter * seq_len] for i in
    out_nrns.tolist()])
learning_signal_out = np.array([events_out['learning_signal'][np.where(senders_out == i)][2:2 + n_iter * seq_len] for i in
    out_nrns.tolist()])
target_rate = np.array([events_out['target_rate'][np.where(senders_out == i)][2:2 + n_iter * seq_len] for i in
    out_nrns.tolist()])
results_out = np.array([times_out, Vms_out, learning_signal_out, target_rate])
np.save('./data/time_traces_out_nest.npy', results_out)

# process data recorded by weight recorder
'''
wr_dict = {
        'rec_nrns': rec_nrns.tolist()
        }
wr_events = wr_in.get('events')
senders = wr_events['senders']
targets = wr_events['targets']
times_wr = wr_events['times']
weights = wr_events['weights']

wr_dict['senders_in'] = senders.tolist()
wr_dict['targets_in'] = targets.tolist()
wr_dict['times_in'] = times_wr.tolist()
wr_dict['weights_in'] = weights.tolist()

wr_events = wr_rec.get('events')
senders = wr_events['senders']
targets = wr_events['targets']
times_wr = wr_events['times']
weights = wr_events['weights']

wr_dict['senders_rec'] = senders.tolist()
wr_dict['targets_rec'] = targets.tolist()
wr_dict['times_rec'] = times_wr.tolist()
wr_dict['weights_rec'] = weights.tolist()

wr_events = wr_out.get('events')
senders = wr_events['senders']
targets = wr_events['targets']
times_wr = wr_events['times']
weights = wr_events['weights']

wr_dict['senders_out'] = senders.tolist()
wr_dict['targets_out'] = targets.tolist()
wr_dict['times_out'] = times_wr.tolist()
wr_dict['weights_out'] = weights.tolist()

datapath = './data/'
filename = 'nest_wr_data_niter{}_seed{}.json'.format(n_iter, rnd_seed)
with open(datapath + filename, 'w') as f:
    json.dump(wr_dict, f)

'''

t_finish = time.time()
print('elapsed time: {}s'.format(datetime.timedelta(seconds=t_finish - t_start)))

